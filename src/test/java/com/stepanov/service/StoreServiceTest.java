package com.stepanov.service;

import com.stepanov.dao.StoreDAO;
import com.stepanov.dto.StoreDTO;
import com.stepanov.util.Generator;
import org.bson.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class StoreServiceTest {
    private StoreService tested;
    @Mock
    private StoreDAO storeDAO;
    @Mock
    private Generator generator;

    @BeforeEach
    void init() {
        tested = new StoreService(storeDAO, generator);
    }

    @Test
    void createIndexTest() {
        tested.createIndex();
        verify(storeDAO, times(1)).createIndex();
    }

    @Test
    void saveStoresTest() {
        List<Document> products = new ArrayList<>();
        Document typeDoc = new Document("_id", 2)
                .append("name", "Меблі");

        Document productDoc = new Document("name", "table")
                .append("type", typeDoc);

        for (int i = 0; i < 10; i++) {
            products.add(productDoc);
        }

        StoreDTO storeDTO = new StoreDTO("bed", "Меблі", 12,
                "Epicentr #3", "Київ, Кільцева дорога, 1-Б");

        when(generator.generateRandomStoreDTO(products)).thenReturn(storeDTO);

        tested.generateAndSave(products, 301, 2);

        verify(storeDAO, times(3)).insert(anyList());
    }


    @Test
    void saveStoresWithArgumentCaptorTest() {
        List<Document> products = new ArrayList<>();
        Document typeDoc = new Document("_id", 2)
                .append("name", "Меблі");

        Document productDoc = new Document("name", "table")
                .append("type", typeDoc);

        for (int i = 0; i < 9; i++) {
            products.add(productDoc);
        }

        StoreDTO storeDTO = new StoreDTO("bed", "Меблі", 12,
                "Epicentr #3", "Київ, Кільцева дорога, 1-Б");

        // what will be captured
        ArgumentCaptor<List<Document>> captor = ArgumentCaptor.forClass(List.class);
        // set mock object`s behavior
        when(generator.generateRandomStoreDTO(anyList())).thenReturn(storeDTO);
        // method invoke
        tested.generateAndSave(products, 301, 2);
        // check whether the argument was captured
        verify(generator, times(301)).generateRandomStoreDTO(captor.capture());
        // get the value of the argument
        List<Document> captorValue = captor.getValue();

        Assertions.assertEquals(9, captorValue.size());

        verify(storeDAO, times(3)).insert(anyList());
    }
}