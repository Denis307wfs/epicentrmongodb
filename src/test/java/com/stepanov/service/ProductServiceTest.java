package com.stepanov.service;

import com.stepanov.dao.ProductDAO;
import com.stepanov.entity.Product;
import com.stepanov.util.Generator;
import org.bson.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {
    private ProductService tested;
    @Mock
    private ProductDAO productDAO;
    @Mock
    private Generator generator;

    @BeforeEach
    void init() {
        tested = new ProductService(productDAO, generator);
    }

    @Test
    void saveProductsTest() {
        List<Product> products = new ArrayList<>();
        Product product = new Product("chair");
        product.setId(5);

        Document typeDoc = new Document("_id", 3).append("name", "Меблі");

        for (int i = 0; i < 30; i++) {
            products.add(product);
        }

        when(generator.generateProducts(100)).thenReturn(products);
        when(generator.getRandomType()).thenReturn(typeDoc);

        tested.generateAndSave(100);

        verify(productDAO, times(1)).insert(anyList());
    }
}