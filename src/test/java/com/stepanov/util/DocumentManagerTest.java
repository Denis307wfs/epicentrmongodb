package com.stepanov.util;

import com.stepanov.dto.StoreDTO;
import com.stepanov.entity.Product;
import com.stepanov.entity.Shop;
import com.stepanov.entity.Type;
import org.bson.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;


class DocumentManagerTest {
    private DocumentManager tested;
    private final String SHOP_CSV = "csv/shop.csv";
    private final String TYPE_CSV = "csv/type.csv";
    private final String[] SHOP_FIELDS = {"_id", "name", "address"};
    private final String[] TYPE_FIELDS = {"_id", "name"};

    @BeforeEach
    void init() {
        tested = new DocumentManager();
    }

    @Test
    void docToShopTest() {
        List<Document> documents = tested.convertCsvToDocuments(SHOP_CSV, SHOP_FIELDS);
        Shop shop = tested.documentToShop(documents.get(0));

        String expectedName = "Epicentr #1";
        String expectedAddress = "Київ, вул. Полярна, 20-Д";

        Assertions.assertEquals(1, shop.getId());
        Assertions.assertEquals(expectedName, shop.getName());
        Assertions.assertEquals(expectedAddress, shop.getAddress());
    }

    @Test
    void docToTypeTest() {
        List<Document> documents = tested.convertCsvToDocuments(TYPE_CSV, TYPE_FIELDS);
        Type type = tested.documentToType(documents.get(10));

        String expectedName = "Електроніка";

        Assertions.assertEquals(11, type.getId());
        Assertions.assertEquals(expectedName, type.getName());
    }

    @Test
    void docToProductTest() {
        Document typeDoc = new Document("_id", 2)
                .append("name", "Меблі");

        Document productDoc = new Document("name", "table")
                .append("type", typeDoc);

        Product product = tested.documentToProduct(productDoc);

        String expectedName = "table";
        String expectedTypeName = "Меблі";
        Assertions.assertEquals(expectedName, product.getName());
        Assertions.assertEquals(2, product.getType().getId());
        Assertions.assertEquals(expectedTypeName, product.getType().getName());
    }

    @Test
    void productToDocTest() {
        Document typeDoc = new Document("_id", 2)
                .append("name", "Меблі");
        Product product = new Product("chair");
        product.setId(5);

        Document productDoc = tested.productToDocument(product, typeDoc);

        String expectedName = "chair";
        String expectedTypeName = "Меблі";
        Assertions.assertEquals(5, productDoc.get("_id"));
        Assertions.assertEquals(expectedName, productDoc.get("name"));

        Document typeDoc2 = (Document) productDoc.get("type");
        Assertions.assertEquals(2, typeDoc2.get("_id"));
        Assertions.assertEquals(expectedTypeName, typeDoc2.get("name"));
    }

    @Test
    void storeDtoToDocTest() {
        String product = "shelf";
        String type = "Меблі";
        int amount = 100;
        String shop = "Epicentr #4";
        String address = "Харків, пр-т Юрія Гагаріна, 352";

        StoreDTO storeDTO = new StoreDTO(product, type, amount, shop, address);
        storeDTO.setId(3);

        Document storeDtoDoc = tested.storeDtoToDocument(storeDTO);

        Assertions.assertEquals(3, storeDtoDoc.get("_id"));
        Assertions.assertEquals(product, storeDtoDoc.get("product"));
        Assertions.assertEquals(type, storeDtoDoc.get("type"));
        Assertions.assertEquals(amount, storeDtoDoc.get("amount"));
        Assertions.assertEquals(shop, storeDtoDoc.get("shop"));
        Assertions.assertEquals(address, storeDtoDoc.get("address"));
    }
}