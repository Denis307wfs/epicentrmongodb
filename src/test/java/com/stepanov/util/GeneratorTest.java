package com.stepanov.util;

import com.stepanov.dto.StoreDTO;
import com.stepanov.entity.Product;
import org.bson.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GeneratorTest {
    private Generator tested;
    @Mock
    private MongoDBManager mongoDBManager;

    @BeforeEach
    void init() {
        tested = new Generator(new MyValidator(), mongoDBManager);
    }

    @Test
    void generateProductsTest() {
        List<Product> products = tested.generateProducts(10);

        Assertions.assertEquals(10, products.size());
    }

    @Test
    void generateStoreDtoTest() {
        List<Document> products = new ArrayList<>();

        Document typeDoc1 = new Document("_id", 2)
                .append("name", "Меблі");
        Document productDoc1 = new Document("name", "table")
                .append("type", typeDoc1);

        products.add(productDoc1);

        Document shopDoc = new Document("_id", 1)
                .append("name", "Epicentr #1")
                .append("address", "Київ, вул. Полярна, 20-Д");

        List<Document> documents = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            documents.add(shopDoc);
        }

        when(mongoDBManager.getDocShops()).thenReturn(documents);

        StoreDTO storeDTO = tested.generateRandomStoreDTO(products);

        Assertions.assertEquals("table", storeDTO.getProduct());
        Assertions.assertEquals("Меблі", storeDTO.getType());
        Assertions.assertTrue(storeDTO.getAmount() <= 1000);
        Assertions.assertEquals("Epicentr #1", storeDTO.getShop());
        Assertions.assertEquals("Київ, вул. Полярна, 20-Д", storeDTO.getAddress());
    }
}