package com.stepanov.util;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.InsertManyResult;
import org.bson.Document;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class MongoDBManagerTest {
    private MongoDBManager tested;
    @Mock
    private MongoDatabase database;
    @Mock
    private MongoCollection<Document> mongoCollection;
    @Mock
    private InsertManyResult insertManyResult;
    private final String SHOP_CSV = "csv/shop.csv";
    private final String TYPE_CSV = "csv/type.csv";
    private final String[] COLLECTIONS = {"shop", "type", "product", "store"};
    private final String[] SHOP_FIELDS = {"_id", "name", "address"};
    private final String[] TYPE_FIELDS = {"_id", "name"};

    @BeforeEach
    void init() {
        tested = new MongoDBManager(database);
        when(database.getCollection(anyString())).thenReturn(mongoCollection);
    }

    @Test
    void createCollectionsTest() {
        tested.createCollections(COLLECTIONS);
        verify(mongoCollection, times(4)).drop();
        verify(database, times(4)).createCollection(anyString());
    }

    @Test
    void insertDataFromShopCSVTest() {
        when(mongoCollection.insertMany(anyList())).thenReturn(insertManyResult);

        tested.insertDataFromFileToTable(SHOP_CSV, "shop", SHOP_FIELDS);
        List<Document> docShops = tested.getDocShops();

        Assertions.assertEquals(10, docShops.size());
        Document document = docShops.get(4);
        String expectedName = "Epicentr #5";
        String expectedAddress = "Харків, вул. Архітекторів, 7";
        Assertions.assertEquals(5, document.get("_id"));
        Assertions.assertEquals(expectedName, document.get("name"));
        Assertions.assertEquals(expectedAddress, document.get("address"));

        Document document2 = docShops.get(9);
        String expectedName2 = "Epicentr #10";
        String expectedAddress2 = "Львів, вул. Богдана Хмельницького, 188-А";
        Assertions.assertEquals(10, document2.get("_id"));
        Assertions.assertEquals(expectedName2, document2.get("name"));
        Assertions.assertEquals(expectedAddress2, document2.get("address"));
    }

    @Test
    void insertDataFromTypeCSVTest() {
        when(mongoCollection.insertMany(anyList())).thenReturn(insertManyResult);

        tested.insertDataFromFileToTable(TYPE_CSV, "type", TYPE_FIELDS);
        List<Document> docTypes = tested.getDocTypes();

        Assertions.assertEquals(15, docTypes.size());

        Document document = docTypes.get(1);
        String expected = "Покриття для підлоги";
        Assertions.assertEquals(2, document.get("_id"));
        Assertions.assertEquals(expected, document.get("name"));

        Document document2 = docTypes.get(14);
        String expected2 = "Одяг, взуття, аксесуари";
        Assertions.assertEquals(15, document2.get("_id"));
        Assertions.assertEquals(expected2, document2.get("name"));
    }
}