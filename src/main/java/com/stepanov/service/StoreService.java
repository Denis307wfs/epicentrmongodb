package com.stepanov.service;

import com.mongodb.client.AggregateIterable;
import com.stepanov.dao.StoreDAO;
import com.stepanov.dto.StoreDTO;
import com.stepanov.util.DocumentManager;
import com.stepanov.util.Generator;
import com.stepanov.util.PropertiesUtil;
import org.apache.commons.lang3.time.StopWatch;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class StoreService {
    private final Logger log = LoggerFactory.getLogger(StoreService.class);
    private final StoreDAO storeDAO;
    private final DocumentManager documentManager;
    private final Generator generator;
    private static final int BATCH_SIZE = Integer.parseInt(PropertiesUtil.get("batchSize"));

    public StoreService(StoreDAO storeDAO, Generator generator) {
        this.storeDAO = storeDAO;
        this.generator = generator;
        this.documentManager = new DocumentManager();
    }

    public void createIndex() {
        log.debug("START CREATING INDEXES\n");
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        storeDAO.createIndex();

        stopWatch.stop();
        double tookTime = (double) stopWatch.getTime() / 1000;
        log.info("INDEX WAS CREATED IN {} s\n", tookTime);
    }

    public void showAddressWithMaxAmountByType(String type) {
        log.debug("START SEARCHING ADDRESS\n");
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        AggregateIterable<Document> result = storeDAO.findAddress(type);
        for (Document doc : result) {
            log.info("Address with max amount products by type ({}):\n{}, amount = {}",
                    type, doc.get("_id"), doc.get("totalAmount"));
        }

        stopWatch.stop();
        double tookTime = (double) stopWatch.getTime() / 1000;
        log.info("SELECT TIME: {} s", tookTime);
    }

    public void generateAndSave(List<Document> products, int rows, int numThreads) {
        log.debug("START INSERTING STORES\n");
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        int numberOfBatches = rows / BATCH_SIZE;
        ExecutorService executorService = Executors.newFixedThreadPool(numThreads);
        insertBatches(products, stopWatch, executorService, numberOfBatches);
        executorService.shutdown();

        // Wait until all tasks are completed or a timeout occurs
        waitAllThreads(executorService);
        insertRemains(products, rows, stopWatch, numberOfBatches);

        stopWatch.stop();
        logResults(rows, stopWatch);
    }

    private void insertBatches(List<Document> products, StopWatch stopWatch,
                               ExecutorService executorService, int numberOfBatches) {
        for (int i = 0; i < numberOfBatches; i++) {
            int batchNum = i;
            executorService.submit(() -> {
                List<Document> documents = new ArrayList<>(BATCH_SIZE);
                int storeId = batchNum * BATCH_SIZE;
                insertBatch(products, BATCH_SIZE, storeId, documents);
                log.debug("BATCH {} with {} rows inserted: {} ms\n",
                        batchNum + 1, BATCH_SIZE, stopWatch.getTime());
                documents.clear();
            });
        }
    }

    private void insertBatch(List<Document> products, int batchSize, int id, List<Document> documents) {
        for (int i = 0; i < batchSize; i++) {
            StoreDTO dto = generator.generateRandomStoreDTO(products);
            dto.setId(++id);
            Document document = documentManager.storeDtoToDocument(dto);
            documents.add(document);
        }
        storeDAO.insert(documents);
    }

    private void waitAllThreads(ExecutorService executorService) {
        try {
            if (!executorService.awaitTermination(5, TimeUnit.MINUTES)) {
                executorService.shutdownNow(); // Forcefully terminate if not finished
            }
        } catch (InterruptedException e) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }

    private void insertRemains(List<Document> products, int rows, StopWatch stopWatch, int numberOfBatches) {
        int remains = rows % BATCH_SIZE;
        if (remains != 0) {
            List<Document> documents = new ArrayList<>(BATCH_SIZE);
            int storeId = numberOfBatches * BATCH_SIZE;
            insertBatch(products, remains, storeId, documents);
            log.debug("REMAIN BATCH with {} rows inserted: {} ms\n\n", remains, stopWatch.getTime());
        }
    }

    private void logResults(int rows, StopWatch stopWatch) {
        double tookTime = (double) stopWatch.getTime() / 1000;
        double rps = rows / tookTime;
        log.info("Inserted {} rows for {} second", rows, tookTime);
        log.info("rps: {}\n", rps);
    }

}
