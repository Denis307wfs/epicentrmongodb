package com.stepanov.service;

import com.stepanov.dao.ProductDAO;
import com.stepanov.entity.Product;
import com.stepanov.util.DocumentManager;
import com.stepanov.util.Generator;
import org.bson.Document;

import java.util.List;
import java.util.stream.Collectors;

public class ProductService {
    private final ProductDAO productDAO;
    private final DocumentManager documentManager;
    private final Generator generator;

    public ProductService(ProductDAO productDAO, Generator generator) {
        this.productDAO = productDAO;
        this.generator = generator;
        this.documentManager = new DocumentManager();
    }

    public void generateAndSave(int numberOfProducts) {
        List<Product> products = generator.generateProducts(numberOfProducts);

        List<Document> documents = products.stream()
                .map(p -> documentManager.productToDocument(p, generator.getRandomType()))
                .collect(Collectors.toList());

        productDAO.insert(documents);
    }

    public List<Document> getAll() {
        return productDAO.getAll();
    }

}
