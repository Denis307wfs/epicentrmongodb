package com.stepanov.dto;

public class StoreDTO {
    private int id;
    private String product;
    private String type;
    private int amount;
    private String shop;
    private String address;

    public StoreDTO(String product, String type, int amount, String shop, String address) {
        this.product = product;
        this.type = type;
        this.amount = amount;
        this.shop = shop;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getShop() {
        return shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
