package com.stepanov.util;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import com.stepanov.dto.StoreDTO;
import com.stepanov.entity.Product;
import com.stepanov.entity.Shop;
import com.stepanov.entity.Type;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

public class DocumentManager {
    private final Logger log = LoggerFactory.getLogger(DocumentManager.class);

    public List<Document> convertCsvToDocuments(String fileName, String[] tableFields) {
        List<Document> documents = new LinkedList<>();

        ClassLoader classLoader = getClass().getClassLoader();

        try (InputStream inputStream = classLoader.getResourceAsStream(fileName);
             CSVReader reader = new CSVReader(new InputStreamReader(inputStream))) {

            int numberOfColumns = tableFields.length;
            String[] line;
            Document document;

            while ((line = reader.readNext()) != null) {
                document = new Document();
                document.append(tableFields[0], Integer.parseInt(line[0]));
                for (int i = 1; i < numberOfColumns; i++) {
                    document.append(tableFields[i], line[i]);
                }
                documents.add(document);
            }
        } catch (FileNotFoundException e) {
            log.error("File {} not found", fileName, e);
        } catch (IOException e) {
            log.error("CSV reader can not read line", e);
        } catch (CsvException e) {
            log.error("Incorrect data in CSV file", e);
        }

        return documents;
    }

    public Shop documentToShop(Document document) {
        return new Shop(document.getInteger("_id"),
                document.getString("name"),
                document.getString("address"));
    }

    public Product documentToProduct(Document document) {
        Product product = new Product(document.getString("name"));
        Document type = (Document) document.get("type");
        product.setType(documentToType(type));
        return product;
    }

    public Type documentToType(Document document) {
        return new Type(document.getInteger("_id"),
                document.getString("name"));
    }

    public Document productToDocument(Product product, Document type) {
        return new Document()
                .append("_id", product.getId())
                .append("name", product.getName())
                .append("type", type);
    }

    public Document storeDtoToDocument(StoreDTO storeDTO) {
        return new Document("_id", storeDTO.getId())
                .append("product", storeDTO.getProduct())
                .append("type", storeDTO.getType())
                .append("amount", storeDTO.getAmount())
                .append("shop", storeDTO.getShop())
                .append("address", storeDTO.getAddress());
    }
}
