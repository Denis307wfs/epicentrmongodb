package com.stepanov.util;

import com.mongodb.MongoCommandException;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class MongoDBManager {
    private final Logger log = LoggerFactory.getLogger(MongoDBManager.class);
    private final MongoDatabase database;
    private final DocumentManager documentManager;
    private List<Document> shops;
    private List<Document> types;

    public MongoDBManager(MongoDatabase database) {
        this.database = database;
        this.documentManager = new DocumentManager();
    }

    public List<Document> getDocShops() {
        return shops;
    }

    public List<Document> getDocTypes() {
        return types;
    }

    public void createCollections(String[] collections) {
        for (String collection : collections) {
            try {
                database.getCollection(collection).drop();
                database.createCollection(collection);
            } catch (MongoCommandException e) {
                log.error("Collection {} does not exist", collection);
            }
        }
    }

    public void insertDataFromFileToTable(String fileName,
                                          String tableName,
                                          String[] tableFields) {
        List<Document> documents = documentManager.convertCsvToDocuments(fileName, tableFields);
        setDocuments(tableName, documents);
        database.getCollection(tableName).insertMany(documents);
    }

    private void setDocuments(String tableName, List<Document> documents) {
        if (tableName.equals("shop")) {
            shops = new ArrayList<>(documents);
        } else if (tableName.equals("type")) {
            types = new ArrayList<>(documents);
        } else {
            log.error("Unknown table {}", tableName);
        }
    }
}