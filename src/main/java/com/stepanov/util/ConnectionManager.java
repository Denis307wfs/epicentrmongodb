package com.stepanov.util;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

public final class ConnectionManager {
    public static final String DB_URL = "db.url";
    public static final String DATABASE = "database";
    public static final String USER_NAME = "username";
    public static final String PASSWORD = "password";
    private static ConnectionManager instance;
    private MongoClient client;

    public static ConnectionManager getInstance() {
        ConnectionManager localInstance = instance;
        if (localInstance == null) {
            synchronized (ConnectionManager.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new ConnectionManager();
                }
            }
        }
        return localInstance;
    }

    public MongoDatabase getDatabase() {
        MongoDatabase mongoDatabase;

        try {
    /*        // for localhost
            client = MongoClients.create(PropertiesUtil.get(DB_URL));
            mongoDatabase = client.getDatabase(PropertiesUtil.get(DATABASE));*/

            // for DocumentDB
            String username = PropertiesUtil.get(USER_NAME);
            String password = PropertiesUtil.get(PASSWORD);
            String clusterEndpoint = PropertiesUtil.get(DB_URL);
            String database = PropertiesUtil.get(DATABASE);

            String template = "mongodb://%s:%s@%s/%s?ssl=true&replicaSet=rs0&readpreference=%s";
            String readPreference = "secondaryPreferred";
            String connectionString = String.format(template, username, password, clusterEndpoint, database, readPreference);

            System.setProperty("javax.net.ssl.trustStore", "my.jks");
            System.setProperty("javax.net.ssl.trustStorePassword", "root1234");

            client = MongoClients.create(connectionString);
            mongoDatabase = client.getDatabase(database);

        } catch (Exception e) {
            throw new IllegalStateException("Cannot obtain a connection", e);
        }

        return mongoDatabase;
    }

}
