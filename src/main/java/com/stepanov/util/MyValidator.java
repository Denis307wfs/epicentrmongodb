package com.stepanov.util;

import com.stepanov.entity.Product;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.hibernate.validator.messageinterpolation.ParameterMessageInterpolator;

import java.util.Set;

public class MyValidator {
    private final Validator validator;
    private Set<ConstraintViolation<Product>> violations;

    public MyValidator() {
        ValidatorFactory factory = Validation.byDefaultProvider()
                .configure()
                .messageInterpolator(new ParameterMessageInterpolator())
                .buildValidatorFactory();
        validator = factory.getValidator();
    }

    public boolean isValid(Product product) {
        violations = validator.validate(product);
        return violations.isEmpty();
    }

    public Set<ConstraintViolation<Product>> getViolations() {
        return violations;
    }
}
