package com.stepanov.util;

import com.stepanov.dto.StoreDTO;
import com.stepanov.entity.Product;
import com.stepanov.entity.Shop;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class Generator {
    private final Logger log = LoggerFactory.getLogger(Generator.class);
    private final Random random;
    private final MyValidator validator;
    private final MongoDBManager mongoDBManager;
    private final DocumentManager documentManager;
    private static final int NAME_MIN_LENGTH = 3;
    private static final int NAME_MAX_LENGTH = 15;
    private static final int MAX_PRODUCT_AMOUNT = 1000;
    private static final int NUMBER_OF_TYPES = Integer.parseInt(PropertiesUtil.get("numberOfTypes"));
    private static final int NUMBER_OF_SHOPS = Integer.parseInt(PropertiesUtil.get("numberOfShops"));

    public Generator(MyValidator validator, MongoDBManager mongoDBManager) {
        random = new Random();
        this.validator = validator;
        this.mongoDBManager = mongoDBManager;
        this.documentManager = new DocumentManager();
    }

    public List<Product> generateProducts(int numberOfProducts) {
        List<Product> products = new ArrayList<>(numberOfProducts);
        log.info("START GENERATING PRODUCTS \n");
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        AtomicInteger id = new AtomicInteger(1);
        Stream.generate(() -> new Product
                        (generateName()))
                .filter(validator::isValid)
                .peek(product -> product.setId(id.getAndIncrement()))
                .limit(numberOfProducts)
                .forEach(products::add);

        stopWatch.stop();
        double tookTime = (double) stopWatch.getTime() / 1000;
        double rps = numberOfProducts / tookTime;
        log.info("Generated {} products for {} second", numberOfProducts, tookTime);
        log.info("rps: {}\n", rps);

        return products;
    }

    private String generateName() {
        int nameLength = random.nextInt(NAME_MAX_LENGTH - NAME_MIN_LENGTH + 1) + NAME_MIN_LENGTH;
        return RandomStringUtils.randomAlphabetic(nameLength);
    }

    public Document getRandomType() {
        int typeId = random.nextInt(NUMBER_OF_TYPES);
        return mongoDBManager.getDocTypes().get(typeId);
    }

    public Document getRandomShop() {
        int typeId = random.nextInt(NUMBER_OF_SHOPS);
        return mongoDBManager.getDocShops().get(typeId);
    }

    public StoreDTO generateRandomStoreDTO(List<Document> products) {
        int amount = random.nextInt(MAX_PRODUCT_AMOUNT) + 1;
        Shop shop = documentManager.documentToShop(getRandomShop());
        int productId = random.nextInt(products.size());
        Product product = documentManager.documentToProduct(products.get(productId));

        return new StoreDTO(
                product.getName(),
                product.getType().getName(),
                amount,
                shop.getName(),
                shop.getAddress()
        );
    }

}
