package com.stepanov;

import com.mongodb.client.MongoDatabase;
import com.stepanov.dao.ProductDAO;
import com.stepanov.dao.StoreDAO;
import com.stepanov.service.ProductService;
import com.stepanov.service.StoreService;
import com.stepanov.util.*;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);
    private static final int THREADS = Integer.parseInt(PropertiesUtil.get("numberOfThreads"));
    private static final int STORES = Integer.parseInt(PropertiesUtil.get("numberOfStores"));
    private static final int PRODUCTS = Integer.parseInt(PropertiesUtil.get("numberOfProducts"));
    private static final String SHOP_CSV = "csv/shop.csv";
    private static final String TYPE_CSV = "csv/type.csv";
    private static final String[] COLLECTIONS = {"shop", "type", "product", "store"};
    private static final String[] SHOP_FIELDS = {"_id", "name", "address"};
    private static final String[] TYPE_FIELDS = {"_id", "name"};

    public static void main(String[] args) {
        LOG.debug("-------START PROGRAM----------\n");

        String type = System.getProperty("type");
        if (type == null || type.isEmpty()) {
            type = "Меблі";
            LOG.info("System param is empty. So type = \"Меблі\"\n");
        }

        ConnectionManager connectionManager = ConnectionManager.getInstance();
        MongoDatabase database = connectionManager.getDatabase();

        MongoDBManager mongoDBManager = new MongoDBManager(database);

        mongoDBManager.createCollections(COLLECTIONS);
        mongoDBManager.insertDataFromFileToTable(SHOP_CSV, "shop", SHOP_FIELDS);
        mongoDBManager.insertDataFromFileToTable(TYPE_CSV, "type", TYPE_FIELDS);

        Generator generator = new Generator(new MyValidator(), mongoDBManager);
        ProductService productService = new ProductService(new ProductDAO(database), generator);

        productService.generateAndSave(PRODUCTS);

        StoreService storeService = new StoreService(new StoreDAO(database), generator);
        List<Document> products = productService.getAll();

        storeService.generateAndSave(products, STORES, THREADS);

        storeService.createIndex();

        storeService.showAddressWithMaxAmountByType(type);

        LOG.debug("-------FINISH PROGRAM----------");
    }
}
