package com.stepanov.dao;

import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.Arrays;
import java.util.List;

import static com.mongodb.client.model.Accumulators.sum;
import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Indexes.ascending;
import static com.mongodb.client.model.Sorts.descending;

public class StoreDAO {
    private final MongoCollection<Document> collection;
    private static final String COLLECTION = "store";

    public StoreDAO(MongoDatabase database) {
        this.collection = database.getCollection(COLLECTION);
    }

    public void insert(List<Document> documents) {
        collection.insertMany(documents);
    }

    public void createIndex() {
        collection.createIndex(ascending("type"));
    }

    public AggregateIterable<Document> findAddress(String type) {
        return collection.aggregate(
                Arrays.asList(
                        match(eq("type", type)),
                        group("$address", sum("totalAmount", "$amount")),
                        sort(descending("totalAmount")),
                        limit(1)
                )
        );
    }

}
