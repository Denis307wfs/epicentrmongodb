package com.stepanov.dao;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.LinkedList;
import java.util.List;

public class ProductDAO {
    private final MongoCollection<Document> collection;
    private static final String COLLECTION = "product";

    public ProductDAO(MongoDatabase database) {
        this.collection = database.getCollection(COLLECTION);
    }

    public void insert(List<Document> documents) {
        collection.insertMany(documents);
    }

    public List<Document> getAll() {
        return collection.find().into(new LinkedList<>());
    }
}
